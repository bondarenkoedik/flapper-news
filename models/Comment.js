"use strict"

let mongoose = require("mongoose");


let CommentSchema = new mongoose.Schema({
  body: String,
  author: String,
  upvotes: {type: Number, default: 0},
  post: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Post"
  }
});

CommentSchema.methods.upvote = function(callback) {
  this.upvotes += 1;
  this.save(callback);
}

let Comment = mongoose.model("Comment", CommentSchema);

module.exports = Comment;