"use strict"

let passport = require("passport");
let LocalStrategy = require("passport-local").Strategy;

let mongoose = require("mongoose");
let User = mongoose.model("User");


passport.use(new LocalStrategy(
  
  function(username, password, done) {
    User.findOne({ username }, (err, user) => {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false, { message: "Incorrect username or password." });
      }
      if (!user.validPassword(password)) {
        return done(null, false, { message: "Incorrect username or password." });
      }
      return done(null, user);
    });
  }

));