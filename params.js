"use strict"

let Post    = require("./models/Post"),
    Comment = require("./models/Comment");

let resolvePost = (req, res, next, postId) => {

  Post.findById(postId, (err, post) => {
    if (err) {
      return next(err);
    } else if (!post) {
      return next(new Error("can not find a post"));
    }
    req.post = post;
    return next();
  });

};

let resolveComment = (req, res, next, commentId) => {

  Comment.findById(commentId, (err, comment) => {
    if (err) {
      return next(err);
    } else if (!comment) {
      return next(new Error("can not find a comment"));
    }
    req.comment = comment;
    return next();
  });

};

module.exports = {
  resolvePost,
  resolveComment
}