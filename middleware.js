"use strict"

let expressJWT = require("express-jwt");


let auth = expressJWT({ secret: "SECRET KEY", userProperty: "payload" }); // the same secret as in User.js

module.exports = {
  auth
}