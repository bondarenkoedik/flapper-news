(function() {

"use strict"

var app = angular.module("flapperNews", ["ui.router"]);

app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {

  $stateProvider

  .state("home", {
    url: "/home",
    templateUrl: "/static/partials/home.html",
    controller: "HomeController",
    resolve: {
      postPromise: ['posts', function(posts) {
        return posts.getAll();
      }]
    },
    // onEnter: ["$state", "auth", function($state, auth) {
    //   if (!auth.isSignedIn()) {
    //     $state.go("posts");
    //   }
    // }]
  })

  .state("posts", {
    url: "/posts/{id}",
    templateUrl: "/static/partials/posts.html",
    controller: "PostController",
    resolve: {
      post: ['$stateParams', 'posts', function($stateParams, posts) {
        return posts.get($stateParams.id);
      }]
    }
  })

  .state("signup", {
    url: "/users/signup",
    templateUrl: "/static/partials/signup.html",
    controller: "AuthController",
    onEnter: ["$state", "auth", function($state, auth) {
      if (auth.isSignedIn()) {
        $state.go("home");
      }
    }]
  })

  .state("signin", {
    url: "/users/signin",
    templateUrl: "/static/partials/signin.html",
    controller: "AuthController",
    onEnter: ["$state", "auth", function($state, auth) {
      if (auth.isSignedIn()) {
        $state.go("home");
      }
    }]
  });

  $urlRouterProvider.otherwise("home");

}]);

app.controller("HomeController", ["$scope", "posts", "auth", function($scope, posts, auth) {

  $scope.posts = posts.posts;

  $scope.addPost = function() {
    if ($scope.title) {
      
      posts.create({
        title: $scope.title,
        link: $scope.link,
      });
      
      $scope.title = '';
      $scope.link = '';
    }
  };

  $scope.incrementUpvotes = function(post) {
    posts.upvote(post);
  };

  $scope.isSignedIn = auth.isSignedIn;

}]);

app.controller("PostController", ["$scope", "posts", "post", "auth", function($scope, posts, post, auth) {

  $scope.post = post;

  $scope.addComment = function() {
    if ($scope.body) {
      
      posts.addComment(post._id, {
        body: $scope.body,
        author: 'user'
      }).success(function(comment) {
        $scope.post.comments.push(comment);
      });
      
      $scope.body = '';
    }
  };

  $scope.incrementUpvotes = function(comment) {
    posts.upvoteComment(post, comment);
  };

  $scope.isSignedIn = auth.isSignedIn;

}]);

app.controller("AuthController", ["$scope", "$state", "auth", function($scope, $state, auth) {

  $scope.user = {};

  $scope.signup = function() {
    auth.signup($scope.user)
      .error(function(err) {
        $scope.error = err;
      })
      .then(function() {
        $state.go("home");
      });
  };

  $scope.signin = function() {
    auth.signin($scope.user)
      .error(function(err) {
        $scope.error = err;
      })
      .then(function() {
        $state.go("home");
      });
  };

}]);

app.controller("NavController", ["$scope", "auth", function($scope, auth) {

  $scope.isSignedIn = auth.isSignedIn;
  $scope.currentUser = auth.currentUser;
  $scope.signout = auth.signout;

}]);

app.factory("posts", ["$http", function($http) {

  return {
    posts: [],
    getAll: function() {
      return $http.get("/posts").success(data => {
        angular.copy(data, this.posts);
      });
    },
    create: function(post) {
      return $http.post("/posts", post).success(function(data) {
        this.posts.push(data);
      });
    },
    upvote: function(post) {
      return $http.put("/posts/" + post._id + "/upvote").success(function(data) {
        post.upvotes += 1;
      });
    },
    get: function(id) {
      return $http.get("/posts/" + id).then(res => {
        return res.data;
      });
    },
    addComment: function(id, comment) {
      return $http.post("/posts/" + id + "/comments", comment);
    },
    upvoteComment: function(post, comment) {
      return $http.put("/posts/" + post._id + "/comments/" + comment._id + "/upvote").success(function(data) {
        comment.upvotes += 1;
      });
    }
  }

}]);

app.factory("auth", ["$http", "$window", function($http, $window) {

  let auth = {};

  auth.saveToken = function(token) {
    $window.localStorage['flapper-new-token'] = token;
  };

  auth.getToken = function() {
    return $window.localStorage['flapper-new-token'];
  };

  auth.isSignedIn = function() {
    let token = auth.getToken();

    if (token) {
      let payload = JSON.parse($window.atob(token.split(".")[1]));
      return payload.exp > Date.now() / 1000;
    }

    return false;
  };

  auth.currentUser = function() {
    if (this.isSignedIn()) {
      
      let token = auth.getToken();
      let payload = JSON.parse($window.atob(token.split(".")[1]));

      return payload.username;
    }
  };

  auth.signup = function(user) {
    return $http.post("/users/signup", user).success(function(data) {
      auth.saveToken(data.token);
    });
  };

  auth.signin = function(user) {
    return $http.post("/users/signin", user).success(function(data) {
      auth.saveToken(data.token);
    });
  };

  auth.signout = function() {
    $window.localStorage.removeItem("flapper-new-token");
  };

  return auth;

}]);

}());