"use strict"

let express = require('express'),
    router  = express.Router();

let Post = require("../models/Post");

let middleware = require("../middleware"),
    params     = require("../params");

router.param("post", params.resolvePost);

router.get("/", (req, res, next) => {

  Post.find({}, (err, posts) => {
    if (err) {
      return next(err);
    } else {
      res.json(posts);
    }
  });

});

router.post("/", middleware.auth, (req, res, next) => {

  const post = req.body;
  post.author = req.payload.username;

  Post.create(post, (err, post) => {
    if (err) {
      return next(err);
    } else {
      res.json(post);
    }
  });

});

router.get("/:post", (req, res, next) => {

  req.post.populate("comments", (err, post) => {
    if (err) {
      return next(err);
    }
    res.json(req.post);
  });

});

router.put("/:post/upvote", middleware.auth, (req, res, next) => {

  req.post.upvote((err, post) => {
    if (err) {
      return next(err);
    }
    res.json(post);
  });

});

module.exports = router;