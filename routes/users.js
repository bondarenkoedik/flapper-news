"use strict"

let express = require('express');
let router = express.Router();

let passport = require("passport");

let User = require("../models/User");


router.post("/signup", function(req, res, next) {

  if (!req.body.username || !req.body.password) {
    return res.status(400).json({ message: "Please, fill out all fields" });
  }

  let user = new User();

  user.username = req.body.username;
  user.setPassword(req.body.password);

  user.save(err => {
    if (err) {
      return next(err);
    }
    return res.json({ token: user.generateJWT() })
  });

});

router.post("/signin", function(req, res, next) {

  if (!req.body.username || !req.body.password) {
    return res.status(400).json({ message: "Please, full out all fields" });
  }

  passport.authenticate('local', (err, user, info) => {

    if (err) {
      return next(err);
    }

    if (user) {
      return res.json({ token: user.generateJWT() });
    } else {
      return res.status(401).json(info);
    }

  })(req, res, next);

});

module.exports = router;
