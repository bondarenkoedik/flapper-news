"use strict"

let express = require("express");
let router = express.Router();

let middleware = require("../middleware");

let Comment = require("../models/Comment");


router.post("/", middleware.auth, (req, res, next) => {

  const comment = req.body;
  comment.author = req.payload.username;

  Comment.create(comment, (err, comment) => {
    if (err) {
      return next(err);
    }
    req.post.comments.push(comment);
    req.post.save((err, post) => {
      if (err) {
        return next(err);
      }
      return res.json(comment);
    });
  });

});

router.put("/:comment/upvote", middleware.auth, (req, res, next) => {
  
  req.comment.upvote((err, comment) => {
    if (err) {
      return next(err);
    }
    res.json(comment);
  });

});

module.exports = router;